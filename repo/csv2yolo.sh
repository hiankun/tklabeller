# FAILED: Cannot find a way to replace `class` with corresponding number...

# csv format:
# img_file,img_width,img_height,label,xmin,ymin,xmax,ymax 
#
# yolo format:
# <object-class> <x_center> <y_center> <width> <height>

folder=$1
label_name=$2

get_label_array () {
	readarray -t label_arr < ${label_name}
	for i in ${!label_arr[@]}; do
		echo $i, ${label_arr[i]}
	done
}


for f in ${folder}*.csv; do
	echo -e "Converting ${f}..."
	yolo_file=`basename $f | sed "s|csv|txt|"`
	awk -F "," '{
		img_width  = $2;
		img_height = $3;
		label      = $4;
		xmin       = $5;
		ymin       = $6;
		xmax       = $7;
		ymax       = $8;

		class    = "xxx";
		x_center = (xmax+xmin)/2/img_width; 
		y_center = (ymax+ymin)/2/img_height; 
		width    = (xmax-xmin)/img_width; 
		height   = (ymax-ymin)/img_height; 
		if (NF == 8) {
			command = "tee '${yolo_file}'"
			printf("%s %.6f %.6f %.6f %.6f\n", 
			class, x_center, y_center, width, height) | command
		} else {
			print "Warning: skip a line..."
		}
	}' \
	${f} 
done
