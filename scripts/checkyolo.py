import cv2
import csv
from pathlib import Path

check_path = "/media/thk/transcend_2T/DataSet/pigs/records-20200723/label_proj_belly/yolo_data/"

COLORS = [
        (255,0,0),
        (0,255,0),
        (0,0,255),
        (255,255,0),
        (255,0,255),
        (0,255,255),
        ]

for anno_f in Path(check_path).glob('*.txt'):
    #FIX: jpg/jpeg
    img_f = check_path + anno_f.stem + '.jpeg'
    img = cv2.imread(img_f)
    height, width, _ = img.shape

    with open(anno_f, 'r') as f:
        annos = csv.reader(f, delimiter=' ')
        for anno in annos:
            label, xc, yc, w, h = [float(_) for _ in anno]
            if xc <= 0 or yc <=0 or w>=width or h>=height:
                print(f'warning at {f}.')
            xmin = int((xc - w/2)*width)
            ymin = int((yc - h/2)*height)
            xmax = int((xc + w/2)*width)
            ymax = int((yc + h/2)*height)
            cv2.rectangle(img, (xmin,ymin), (xmax,ymax), COLORS[int(label)], 2)

    cv2.imshow('img', img)

    if cv2.waitKey(0) & 0xff == ord('q'):
        break

cv2.destroyAllWindows()
