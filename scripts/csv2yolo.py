# csv format:
# img_file,img_width,img_height,label,xmin,ymin,xmax,ymax 
#
# yolo format:
# <object-class> <x_center> <y_center> <width> <height>

import csv
import argparse
from pathlib import Path
import logging
import sys

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s:[%(levelname)s] %(message)s')
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)


class CSV2YoloConverter():
    def __init__(self, proj_path):
        label_file = proj_path/"labels"
        self.out_path = proj_path/"yolo_annos/"
        with open(label_file, 'r') as labels:
            self.label_list = [label.rstrip() for label in labels]
        self.csv_fname = None

        try:
            self.out_path.mkdir(parents=True, exist_ok=False)
            logger.info(f"Output folder '{self.out_path}' "
                    "has been created...")
        except FileExistsError:
            logger.warning(f"Output folder '{self.out_path}' exist.\n"
                    "\tDo you want to use the same output folder?")
                    
            user_input = input("\t'Y' to continue, other keys to stop. ")
            if user_input.upper() == 'Y':
                self.out_path.mkdir(parents=True, exist_ok=True)
                logger.info(f"Output folder '{self.out_path}' "
                        "has been created...")
            else:
                sys.exit()


    def convert_format(self, csv_data):
        _, _img_w, _img_h, label, *_bbox = csv_data
        img_w = int(_img_w)
        img_h = int(_img_h)
        xmin,ymin,xmax,ymax = [int(pt) for pt in _bbox]
    
        obj_class = self.label_list.index(label)
        x_center  = (xmax + xmin)/2.0/img_w
        y_center  = (ymax + ymin)/2.0/img_h
        width     = (xmax - xmin)/img_w
        height    = (ymax - ymin)/img_h

        data_str = (f'{obj_class} {x_center:.4f} {y_center:.4f} '
                f'{width:.4f} {height:.4f}')
        self.yolo_data_list.append(data_str)
    
    def write_output(self):
        out_fname = self.out_path/Path(self.csv_fname.stem + ".txt")
        with open(out_fname, 'w') as f:
            f.writelines(
                    [f'{_data}\n' for _data in self.yolo_data_list])
    
    def get_yolo_format(self, csv_fname):
        self.yolo_data_list = []
        self.csv_fname = csv_fname
        with open(self.csv_fname, 'r') as f:
            anno_data = csv.reader(f, delimiter=',')
            for _data in anno_data:
                if len(_data) == 8:
                    self.convert_format(_data)
                else:
                    logger.warning("skipped an unmatched line...")
        if not self.yolo_data_list:
            logger.warning(f"no available output for {self.csv_fname}.")
        else:
            self.write_output()


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--proj_path', required=True,
            help="Project path containing the csv annotations.")
    return parser.parse_args()

def main():
    args = get_args()
    proj_path = Path(args.proj_path)
    input_folder = proj_path/"annos/"

    c2y = CSV2YoloConverter(proj_path)

    for csv_f in Path(input_folder).glob("*.csv"):
        c2y.get_yolo_format(csv_f)


if __name__=='__main__':
    main()
