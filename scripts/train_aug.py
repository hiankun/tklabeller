from PIL import Image, ImageDraw
from pathlib import Path
import numpy as np
import argparse
import sys
import csv
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(levelname)s:%(message)s')
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)


class SimpleAugmentation():
    def __init__(self, args):
        self.args = args
        if self.args.output_path:
            self.aug_path = Path(self.args.output_path).resolve()
        else:
            proj_path = Path(self.args.input_path).resolve()
            proj_name = proj_path.name
            self.aug_path = Path(proj_path.parent)/(proj_name + '-aug')
        self.aug_images_path = Path(self.aug_path)/'images'
        self.aug_annos_path = Path(self.aug_path)/'annos'
        try:
            self.aug_images_path.mkdir(parents=True, exist_ok=False)
            self.aug_annos_path.mkdir(parents=True, exist_ok=False)
            logger.debug('Created augmented images folder:\n  {}'.format(
                self.aug_images_path))
            logger.debug('Created augmented annotations folder:\n  {}'.format(
                self.aug_annos_path))
        except:
            logger.error('Cannot create\n  {}\nand/or\n  {}.\n' \
                    'Please check whether they are already in the path'.format(
                        self.aug_images_path, self.aug_annos_path))
            sys.exit()


    def clean_up(self):
        try:
            self.aug_images_path.rmdir()
            self.aug_annos_path.rmdir()
            self.aug_path.rmdir()
        except:
            logger.debug('Augmented folders are not empty...')


    def get_data(self, anno_data):
        rois = []
        #for _data in anno_data:
        for _data in anno_data:
            self.img_file, _w, _h, *label_pts = _data
            rois.append(label_pts)
        image = np.array(Image.open(self.img_file))
        return image, rois

    
    def check_rect(self, aug_roi):
        """ Rearrange the rectangle corners to [xmin,ymin,xmax,ymax] """
        label,x1,y1,x2,y2 = aug_roi
        if x1 > x2:
            x1,x2 = x2,x1
        if y1 > y2:
            y1,y2 = y2,y1
        return [label,x1,y1,x2,y2]
    

    def set_transform(self, param):
        a,b,tx,c,d,ty = param
        return np.array([[a,b,tx],[c,d,ty]])

    
    def apply_transform(self, transform=None, rois=None, mode=None):
        aug_rois=[]
        for roi in rois:
            aug_roi = [roi[0]] # the label
            for x,y in zip(roi[1::2], roi[2::2]):
                x,y = np.dot(transform, [int(x),int(y),1])
                aug_roi.extend([x,y])
            if len(aug_roi) == 5: # label + 2pts
                aug_roi = self.check_rect(aug_roi)
            aug_rois.append(aug_roi)
        return aug_rois

    def do_aug(self, img, rois, mode):
        method = {
                'hflip':np.fliplr,
                'vflip':np.flipud,
                'rotate90':np.rot90,
                }
        aug_img = method[mode](img)
        h, w, _ = aug_img.shape
        trans_mat = {
                'hflip':(-1,0,w,0,1,0),
                'vflip':(1,0,0,0,-1,h),
                'rotate90':(0,1,0,-1,0,h),
                }
        trans_mat = self.set_transform(trans_mat[mode])
        aug_rois = self.apply_transform(
                transform=trans_mat, rois=rois, mode=mode)
        return aug_img, aug_rois


    def save_aug_results(self, img, rois, tag):
        # save image
        f_name = Path(self.img_file)
        ext = f_name.suffix
        self.aug_img_file = Path(self.aug_images_path)/(
                f_name.stem + '-aug-' + tag + ext)
        self.aug_anno_file = Path(self.aug_annos_path)/(
                f_name.stem + '-aug-' + tag + '.csv')
    
        aug_img = Image.fromarray(img)
        aug_img.save(self.aug_img_file)

        # save annotations
        w, h = aug_img.size
        with self.aug_anno_file.open(mode='w') as f:
            for roi in rois:
                _anno = '{},{},{},{}\n'.format(self.aug_img_file, w, h, roi)
                aug_anno = ''.join(
                        _ for _ in _anno if _ not in ("["," ","]","'"))
                logger.debug(aug_anno)
                f.write(aug_anno)

        logger.info('Augmented files {} and {} have been saved in {}'.format(
            self.aug_img_file.name, self.aug_anno_file.name, self.aug_path))
    

    def show_aug_results(self, img, rois):
        aug_img = Image.fromarray(img)
        draw = ImageDraw.Draw(aug_img)
        for roi in rois:
            pts = roi[1:] # exclude label
            if len(pts) == 4:
                draw.rectangle(pts, outline=(255,255,0))
            else:
                draw.polygon(pts, outline=(255,255,0))
        aug_img.show()

    def get_train_csv_files(self, train_data):
        csv_files = []
        for data in train_data:
            csv_files.append(Path(data[0].replace('images/','annos/')).with_suffix('.csv'))
        return set(csv_files)


def parse_args():
    description_text = 'Augmented images and corresponding annotations will \
            be saved in "<output_path>/images" and "<output_path>/annos", \
            respectively. If the augmentation fails to output any files, \
            then the above folders will be removed automatically.'
    parser = argparse.ArgumentParser(description=description_text)
    parser.add_argument('-i', '--input_file', required=True,
            help='The splitted train.csv')
    parser.add_argument('-o', '--output_path', required=False, default=None,
            help='Output path. If not given, an "augmented" folder named \
                    "<input_path>-aug" will be created alongside the \
                    <input_path> folder.')
    return parser.parse_args()


def main():
    args = parse_args()

    sa = SimpleAugmentation(args)
    train_file = args.input_file

    # Collect the filenames in the train set,
    with open(train_file, 'r') as f:
        train_data = csv.reader(f, delimiter=',')
        _header = next(train_data)
        csv_files = sa.get_train_csv_files(train_data)

    # Augment them
    for anno_file in csv_files:
        with open(anno_file, 'r') as f:
            anno_data = csv.reader(f, delimiter=',')
            img_orig, rois_orig = sa.get_data(anno_data)
        # Do the augmentation (rotation only)
        img_aug_r090, rois_aug_r090 = sa.do_aug(img_orig, rois_orig, mode='rotate90')
        sa.save_aug_results(img_aug_r090, rois_aug_r090, tag='r090')

        img_aug_r180, rois_aug_r180 = sa.do_aug(img_aug_r090, rois_aug_r090, mode='rotate90')
        sa.save_aug_results(img_aug_r180, rois_aug_r180, tag='r180')

        img_aug_r270, rois_aug_r270 = sa.do_aug(img_aug_r180, rois_aug_r180, mode='rotate90')
        sa.save_aug_results(img_aug_r270, rois_aug_r270, tag='r270')


        # Do the augmentation (add vflip)
        img_aug_vf, rois_aug_vf = sa.do_aug(img_orig, rois_orig, mode='vflip')
        sa.save_aug_results(img_aug_vf, rois_aug_vf, tag='vf')
        
        img_aug_r090_vf, rois_aug_r090_vf = sa.do_aug(img_aug_r090, rois_aug_r090, mode='vflip')
        sa.save_aug_results(img_aug_r090_vf, rois_aug_r090_vf, tag='r090_vf')

        img_aug_r180_vf, rois_aug_r180_vf = sa.do_aug(img_aug_r180, rois_aug_r180, mode='vflip')
        sa.save_aug_results(img_aug_r180_vf, rois_aug_r180_vf, tag='r180_vf')

        img_aug_r270_vf, rois_aug_r270_vf = sa.do_aug(img_aug_r270, rois_aug_r270, mode='vflip')
        sa.save_aug_results(img_aug_r270_vf, rois_aug_r270_vf, tag='r270_vf')


        # When working with very small set of augmented images, you may use the 
        # following function to check the results.
        #sa.show_aug_results(img_aug_v, rois_aug_v)

    sa.clean_up()


if __name__ == '__main__':
    main()


## Do the augmentation (single step)
#img_aug_h, rois_aug_h = sa.do_aug(img_orig, rois_orig, mode='hflip')
#sa.save_aug_results(img_aug_h, rois_aug_h, tag='h')
#img_aug_v, rois_aug_v = sa.do_aug(img_orig, rois_orig, mode='vflip')
#sa.save_aug_results(img_aug_v, rois_aug_v, tag='v')
#img_aug_r, rois_aug_r = sa.do_aug(img_orig, rois_orig, mode='rotate90')
#sa.save_aug_results(img_aug_r, rois_aug_r, tag='r')

## Do the augmentation (multiple steps)
#img_aug_hv, rois_aug_hv = sa.do_aug(
#        img_aug_h, rois_aug_h, mode='vflip')
#sa.save_aug_results(img_aug_hv, rois_aug_hv, tag='hv')
#img_aug_hr, rois_aug_hr = sa.do_aug(
#        img_aug_h, rois_aug_h, mode='rotate90')
#sa.save_aug_results(img_aug_hr, rois_aug_hr, tag='hr')
#img_aug_hrh, rois_aug_hrh = sa.do_aug(
#        img_aug_hr, rois_aug_hr, mode='hflip')
#sa.save_aug_results(img_aug_hrh, rois_aug_hrh, tag='hrh')
#img_aug_vr, rois_aug_vr = sa.do_aug(
#        img_aug_v, rois_aug_v, mode='rotate90')
#sa.save_aug_results(img_aug_vr, rois_aug_vr, tag='vr')

