The format of tkl annotation files are based on the need of converting to
TensorFlow TFRecords (and I think it's easier than XML files for human to read).

For a rectangle or square annotation, the format is in the form of
```
image-path, image-width, image-height, label-name, xmin, ymin, xmax, ymax
```
where the (xmin, ymin) and (xmax, ymax) are the top-right corner and the
bottom-left corner of the rectangle or square, respectively.

For example, here is one instance in the annotation file:
```
/<some_path>/proj-02/images/twice01.jpeg,1038,811,Jeongyeon,566,385,681,548
```

For a polygon annotation, the format is almost the same except for the coordinates
part [1]:
```
image-path, image-width, image-height, label-name, x1, y1, x2, y2, ..., xn, yn
```

The following example shows an annotation with a set of polygon coordinate.
```
/<some_path>/proj-02/images/twice01.jpeg,1038,811,Jihyo,223,389,211,389,197,400,196,406,190,413,187,422,169,446,168,484,171,489,169,497,171,507,178,521,176,525,200,536,203,534,220,535,223,533,229,535,240,529,261,505,270,490,270,487,268,486,268,477,270,474,269,451,246,431,231,394,223,389
```

You can check the examples in the folder of [proj-01/annos/](../proj-01/annos/) 
and [proj-02/annos/](../proj-02/annos/).

---

[1] Recently I am working on training Mask-RCNN with multiple classes.
    When preparing the training data, I learned something about masks in the form of
    PNG images.
    I would add some functions to convert the polygons to PNG masks.
