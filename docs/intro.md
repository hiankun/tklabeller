This is a simple labelling/annotation tool written in Python 3 and Tkinter.
Its name is `tkLabeller` and you can just call it `tkl` (pronounced as "tickle" [1]).

There are many great open-sourced tools for image labelling/annotation,
but I cannot find one which fit all my needs.
Of course I could modify the code of those great tools, only if my coding level
is good enough to do so.

I am a Vim user and trust keyboard more than mouse.
However, image annotation is a task which is impossible (or extremely difficult?)
to do using pure CLI.
Nevertheless, the existing tools are still using too much mouse clicks,
many of which could/should be reduced by simple keyboard strokes
or some internal mechanisms designed in the labelling tool.

Finally, I want to exercise my embarrassing Tkinter and OOP programming skills.
Therefore, in the beginning of 2020,
I decided to write a labelling tool to make my labelling works easier.

The main functions have been done in April, but ideas keep coming up,
and bugs as well as some UX considerations are always changing.
Although the code is not clean and some minor tweaks are necessary,
I still want to make tkl public now.

---

[1] AFAIK, Tcl language is pronounced "tickle". Although I know almost nothing about Tcl,
but I still want to call `tkl` "tickle" to pay tribute to Tcl/Tk.
Also, I like [64 Zoo Lane](https://en.wikipedia.org/wiki/64_Zoo_Lane),
which was one of my son's favorite cartoon when he was a little boy.
Giggles and Tickles are two main characters in 64 Zoo Lane...
So here comes the name `tickle`. ;-)

