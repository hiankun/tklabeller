__NOTE__: The documentation is under construction.

![](docs/pics/tklabel_twice.gif)

# Introduction

This is a simple labelling/annotation tool written in Python 3 and Tkinter.

[Read more](./docs/intro.md)

# Features

I write this tool with "labelling things quickly and easily" in mind.
The features are:

- [x] Minimal: Use Tkinter so that you don't need to install other stuff.
- [x] Wrist and forearm friendly: Utilize keyboard shortcuts to reduce or 
      eliminate unnecessary mouse usage.
- [x] No need to press-and-hold mouse buttons: Take rectangle as the example,
      you click to select the first point and move the mouse freely to the
      second point, then click again to complete the rectangle.
      Your finger(s) will appreciate this feature.
- [x] Labelled objects counting: Show the numbers of labelled objects along 
      with their class names, so you can easily know how many objects of each 
      classes have been labelled.
- [x] Configurable: You can change line width and colors, or change keyboard 
      shortcuts to fit your need. (see `config.yaml`)
- [x] Experimental: Use GrabCut to generate polygons (disabled by default). 
      If you want to try it, then you have to install `opencv-python`.

# TODOs

Some features which are very common in other annotation tools but are not available 
in `tkl` yet. They are:
- [ ] Resize the image when labelling.
- [ ] Modify the labelled shapes (rectangles, squares, and polygons).

# What is tkl used for?

Like many other labelling tools used in Deep Learning working pipeline,
`tkl` is created to help you labelling your images with boxes or polygons 
which is necessary to generate the training/validation annotation files.

# System requirements

* OS: So far I have only tested tkl in Lubuntu 18.04 and Windows 10. 
  Other platforms which can install the following packages might be okay.
* Python 3.6 and above (I used f-Strings in the code)
* Optional: OpenCV for Python (for GrabCut functions)

# Basic usage

## Project structure

You have to prepare the following folders and files for tkl to work.

```
proj-name
├── annos     (for a new project, this might be an empty folder)
│   └── *.csv (tkl will generate these files, or you should put your 
│              annotation files which match the format[1])
├── images
│   └── *.jpg (you have to put your images here)
└── labels    (and also prepare the class names in this file)
```

You can freely name your own `proj-name`, but the folder names of `annos` and 
`images` and the file name of `labels` are restricted by the values written in
`config.yaml`.
So if you want to change the names, remember to change them in the `config.yaml`
file accordingly.

## Open your project

There are two ways to open your project with tkl.

Just run `python tklabeller.py` to bring up tkl GUI and `Ctrl+O` to open the
file dialogue, then go to your project folder (you should see `annos` and
`images` folders are already there) and click `Ok`.

You can also write the absolute path of your project in `config.yaml` as the 
follows:
```
proj_path: /your_root_path/tkLabeller/proj-name/
```
By doing so, you don't need to search your project whenever you entering tkl. 
Instead, tkl will bring you to your project folder after `Ctrl+o`.

## Navigation and controls

After loading the project, you can use the following default keys to navigate:

* Next image: `j`
* Previous image: `k`
* Quit: `Ctrl+q`

## Label the images

1. Select tools in `Annotation Tools` panel.
2. Select label name in `Class Labels` list.
3. Begin to label.

The default keys used to annotate the images:

* Save (<b>u</b>pdate) the annotations to CSV files: `u`
* Delete (k<b>i</b>ll) the selected ROI(s): `i`
* Toggle <b>l</b>abelling text on/off: `l`
* Toggle guiding <b>c</b>ross on/off: `c`

# Use GrabCut to generate polygon

This is an experimental function which is used to reduce the manual effort when
labelling objects using polygons.

1. Select `GrabCut` from `Annotation Tools` panel.
2. Select rectangular ROI on the target object of the image.
3. The mask will be generated automatically.
   * You can press `g` key to make GrabCut iterate more times.
   * Sometimes GrabCut cannot get foreground mask and you have to set it manually.
4. You can choose `FG` to bring back some regions being treated as background;
   the `BG` is used for the opposite purpose.
5. When you are satisfied with the result, press `p` to convert the mask into
   polygon. Done.

# Setting up your own configuration

(TODO)

# Simple augmentation

(TODO)

---
[1] [The format](./docs/anno_format.md) of tkl annotation files.
