import os
from glob import glob
import csv
import yaml
import colorsys
from colours import COLOURS, ROI_COLOURS


def reset_config():
    with open('config.yaml', encoding='utf-8', mode='r') as f:
        config = yaml.safe_load(f)
    return config

config = reset_config()

class GrabCut:
    do_grabcut = config['do_grabcut']
    add_gc_poly = config['add_gc_poly']


class Control:
    next_file = config['next_file']
    prev_file = config['prev_file']
    update_anno = config['update_anno']
    kill_roi = config['kill_roi']
    canvas_left  = config['canvas_left']
    canvas_down  = config['canvas_down']
    canvas_up    = config['canvas_up']
    canvas_right = config['canvas_right']
    guiding_cross = config['guiding_cross']
    labels_visible = config['labels_visible']


class Common:
    window_mode = config['window_mode']
    window_size = config['window_size']
    start_from_last = config['start_from_last']
    auto_grabcut = config['auto_grabcut']
    last_f_idx = config['last_file_index']
    proj_path = config['proj_path']

    img_dir = config['img_dir']
    img_ext = config['img_ext']
    closeness = 5 #px
    fixed_radius = config['fixed_radius'] #px

    label_filename = config['label_filename']
    anno_dir = config['anno_dir']

    tool_mode = {
            'Rectangle': 0,
            'Square': 1,
            'FixedSquare': 2,
            'Polygon':3,
            }
    grabcut_mode = {
            'GrabCut':4,
            'BG':5,
            'FG':6,
            'PR_BG':7,
            'PR_FG':8,
            }
    tool_set = [
       ("Rectangle",   tool_mode['Rectangle']),
       ("Square",      tool_mode['Square']),
       ("FixedSquare", tool_mode['FixedSquare']),
       ("Polygon",     tool_mode['Polygon']),
       ]
    grabcut_set = [
       ("GrabCut",     grabcut_mode['GrabCut']),
       ("  BG",        grabcut_mode['BG']),
       ("  FG",        grabcut_mode['FG']),
       ("  PR_BG",     grabcut_mode['PR_BG']),
       ("  PR_FG",     grabcut_mode['PR_FG']),
       ]

    if config['use_grabcut']:
        tool_mode.update(grabcut_mode)
        tool_set.extend(grabcut_set)

class Info:
    tool_info = " Key strokes usage \n"\
               +"===================\n"\
               +"{}: Next file\n".format(Control.next_file)\
               +"{}: Previous file\n".format(Control.prev_file)\
               +"{}: Write annota-\n".format(Control.update_anno)\
               +"   tions to *.csv\n"\
               +"{}: Delete chosen\n".format(Control.kill_roi)\
               +"   ROI(s)\n"\
               +"Esc: Cancel point\n"\
               +"\n"\
               +"{}: Toggle guiding\n".format(Control.guiding_cross)\
               +"   cross on/off\n"\
               +"{}: Toggle labels\n".format(Control.labels_visible)\
               +"   visible on/off\n"\
               +"-------------------\n"
    if config['use_grabcut']:
        tool_info += "{}: Run GrabCut\n".format(GrabCut.do_grabcut)\
                    +"{}: Add GrabCut mask\n".format(GrabCut.add_gc_poly)\
                    +"   to polygon\n"\
                    +"-------------------\n"

    usage_text = "This is a simple usage guide.\n\n"\
        + "1. Prepare your 'images' and 'annos' folders and 'labels' file."\
        +" (Examples can be found in config.yaml.)\n"\
        + "2. Ctrl+O to open your project.\n"\
        + "3. Select the tool from Annotation Tools (left-hand side panel).\n"\
        + "4. Select the label from Class Labels (right-hand side panel).\n"\
        + "5. Click to set the label area around the object.\n"\
        + "6. Press 'u' to save the labelling results.\n\n"\
        + "Once you have wrong labelling:\n"\
        + "* Right click in the labelled area to select it,"\
        + " and then press 'i' to delete.\n"\
        + "* Double right click in the labelled area to change the label name."\


    about_text = "tkLabeller (2020-2021)\n"\
            + "author: Hian-Kun Tenn\n"\
            + "code: https://gitlab.com/hiankun/tklabeller\n"


class Draw:
    canvas_bg_color = '#0a082e'
    cross_line_color = COLOURS[10] #'#aa9cff'
    cross_line_width = 1
    roi_line_color = ROI_COLOURS #FIX: when idx out of range? #'#aacc00'
    roi_line_width = 2
    brush_size = 3
    selected_text_color = 'yellow'
    selected_line_color = 'red'
    selected_line_width = 3
    selected_roi = []
    enable_guiding_cross = True # used for overall control
    draw_guiding_cross = True # always True; used for temporary control
    enable_label_visible = True # used for overall control

    status_color = {
            0: ('RoyalBlue4', 'white'),
            1: ('slate blue', 'linen'),
            2: ('gray9', 'snow'),
            }

    # GrabCut colors
    # Do NOT change 'val' values, which've been defined in OpenCV.
    mask_BG    = {'color':'#ff0000', 'val':0}
    mask_FG    = {'color':'#00ff00', 'val':1}
    mask_PR_BG = {'color':'#ff9933', 'val':2}
    mask_PR_FG = {'color':'#66ff66', 'val':3}

    def hex_inverter(color):
        r,g,b = tuple(int(color.lstrip('#')[i:i+2], 16)/255 for i in (0,2,4))
        h,l,s = colorsys.rgb_to_hls(r,g,b)
        h = h+0.5 if h <=0.5 else h-0.5
        l = 1.0 if l <=0.5 else 0.0
        s = s+0.5 if s <=0.5 else s-0.5
        r,g,b = [int(_*255) for _ in colorsys.hls_to_rgb(h,l,s)]
        #r,g,b = [255,255,255] if l<0.6 and h>0.6 else [0,0,0]
        color = '#{:02x}{:02x}{:02x}'.format(r,g,b)
        return color

