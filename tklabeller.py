# https://stackoverflow.com/a/42008816
# > tkinter is more like a lumber store than a furniture store. 
# > You can create really nice interfaces with Tkinter, but you 
# > sometimes have to do the work yourself. – Bryan Oakley Feb 2 '17 at 17:46

import tkinter as tk
from tkinter.filedialog import (
        askdirectory, 
        asksaveasfile,
        askopenfilename)
from tkinter import messagebox
from PIL import Image, ImageTk, ImageDraw, ImageColor
from pathlib import Path
import itertools
import csv
from settings import reset_config, Control, Common, Info, Draw, GrabCut
from collections import Counter
import logging
import time
import datetime
import math
import shutil
import re
from fnmatch import fnmatch
config = reset_config()
if config['use_grabcut']:
    import cv2
import numpy as np
import webbrowser

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s:[%(levelname)s] %(message)s')
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)

class DoGrabCut():
    def __init__(self, *args, **kwargs):
        self.bgdmodel = np.zeros((1, 65), np.float64)
        self.fgdmodel = np.zeros((1, 65), np.float64)
        self.epsilon = None
        self.polygon = []
        self.mask = None
        self.res_mask = None

    def cal_mask(self, mode, ref_rect=None, mask=None, img_file=None):
        self.polygon = []
        self.mask = mask
        self.img_file = img_file
        self.img = cv2.imread(self.img_file)
        if mode == Common.grabcut_mode['GrabCut']:
            self.method = cv2.GC_INIT_WITH_RECT
            x1,y1,x2,y2 = ref_rect
            self.rect = [x1, y1, x2-x1, y2-y1]
            self.mask = np.zeros(self.img.shape[:2], np.uint8)
        else:
            self.method = cv2.GC_INIT_WITH_MASK
            self.rect = None
        cv2.grabCut(self.img, self.mask, self.rect, 
                self.bgdmodel, self.fgdmodel, 1, self.method)
        res_mask = np.where(
                (self.mask==1) + (self.mask==3), 255, 0).astype('uint8')
        if np.count_nonzero(res_mask) == 0:
            logger.warning('cal_mask: No FG found. Please set it manually...')
            return self.mask, self.res_mask, None
        self.get_polygon(res_mask)
        self.res_mask = res_mask
        return self.mask, self.res_mask, self.polygon

    def get_polygon(self, mask):
        cnts, _ = cv2.findContours(mask,
                cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnt = max(cnts, key=cv2.contourArea)
        epsilon = 0.003*cv2.arcLength(cnt, True)
        approx = cv2.approxPolyDP(cnt, epsilon, True)
        approx = np.squeeze(approx, axis=1)
        self.polygon = np.append(approx, [approx[0]], axis=0)


class App(tk.Tk):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title("Tk Labeller")
        if Common.window_mode == 0:
            self.attributes('-fullscreen', True)
        elif Common.window_mode == 1:
            _w,_h = Common.window_size
            #self.minsize(_w,_h)
            self.geometry("%dx%d+0+0" % (_w, _h))
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

        # Create the menu
        self.menubar = AppMenu(self)
        self.config(menu=self.menubar)
        self.bind('<Control-o>', self.menubar.open_proj)
        self.bind('<Control-s>', self.menubar.saveas_file)
        #self.bind('<Control-q>', quit)
        self.bind('<Control-q>', self.check_before_quit)

        # Status Bar
        self.statusbar = StatusBar(self)
        self.statusbar.pack(side='bottom',fill='x',expand=False) 

        # Label Listbox
        self.listboxframe = ListboxFrame(self)
        self.listboxframe.pack(side='right',fill='y')

        # Canvas
        self.imagecanvas = ImageCanvas(self)
        self.imagecanvas.pack(side='right',fill='both',expand=True,
                padx=(5,0),pady=(5,0))
        self.bind(Control.guiding_cross, self.toggle_guiding_cross)
        self.bind(Control.labels_visible, self.toggle_labels_visible)

        # Tool Panel
        self.toolpanel = ToolPanel(self)
        self.toolpanel.pack(side='top',fill='y',expand=False) 
        
        # Navigation Panel
        navigationpanel = NavigationPanel(self)
        self.bind(Control.prev_file, navigationpanel.prev_file)
        self.bind(Control.next_file, navigationpanel.next_file)
        navigationpanel.pack(side='bottom',fill='x',expand=False) 

        # GrabCut
        self.gc = DoGrabCut(self)
        # File settings
        self.img_list = []
        self.f_idx = 0
        self.f_len = 0
        self.img_file = None
        self.current_anno = None

    def update_config(self):
        re_pattern = re.compile("^last_file_index:\s*\d+")
        shutil.copy2('config.yaml', 'config.yaml.bak')
        with open('config.yaml.bak', mode='r', encoding='utf-8') as f_in:
            last_f_idx = f'last_file_index: {self.f_idx}'
            with open('config.yaml', mode='w', encoding='utf-8') as f_out:
                for line in f_in:
                    f_out.write(re.sub(re_pattern, last_f_idx, line))

    def check_before_quit(self, event='check_before_quit'):
        self.update_config()
        if self.imagecanvas.anno_dict_changed:
            ret = messagebox.askyesnocancel("ROI changed", 
                "Some annotations have been changed. Save them before quit?"
                + "(Cancel to stay here...)")
            if ret is True:
                logger.debug('check_before_quit: save then Quit...')
                self.imagecanvas.save_annotations()
                self.quit()
            elif ret is False:
                logger.debug('check_before_quit: Quit without saving...')
                self.quit()
            else: # user cancel it; stay in the same frame
                return False
        else:
            self.quit()

    def toggle_guiding_cross(self, event='toggle_guiding_cross'):
        Draw.enable_guiding_cross = not Draw.enable_guiding_cross
        self.imagecanvas.canvas.delete('guiding_cross')
        if Draw.enable_guiding_cross:
            logger.info('Guiding cross ON')
        else:
            logger.info('Guiding cross OFF')

    def toggle_labels_visible(self, event='toggle_labels_visible'):
        Draw.enable_label_visible = not Draw.enable_label_visible
        label_rect_pattern = 'label_rect_*'
        label_text_pattern = 'label_text_*'
        state = 'hidden'
        for _id in self.imagecanvas.canvas.find_all():
            if any(fnmatch(tag, label_rect_pattern) 
                    for tag in self.imagecanvas.canvas.gettags(_id)) \
                or any(fnmatch(tag, label_text_pattern) 
                    for tag in self.imagecanvas.canvas.gettags(_id)):
                state = (self.imagecanvas.canvas.itemcget(_id,'state'))
                if state == 'normal':
                    state = 'hidden'
                else:
                    state = 'normal'
                self.imagecanvas.canvas.itemconfigure(_id, state=state)
        if state == 'hidden':
            logger.info('All labels have been hidden...')
        else:
            logger.info('All labels are visible now...')

    def set_file2canvas(self):
        try:
            _img_file = self.img_list[self.f_idx]
        except:
            messagebox.showwarning("Cannot load the image", 
                "The 'last_file_index' given by 'config.yaml' might be not valid.\n"
                "The program will try to load the first image.")
            self.f_idx = 0
            _img_file = self.img_list[self.f_idx]
        self.imagecanvas.load_image(_img_file)

        # Use resolve() to get softlink target (the real image path).
        # NOTE: Convert file path to string or anno_dict_changed() will fail.
        self.img_file = str(Path(self.img_list[self.f_idx]).resolve())

        anno_file = Path(self.img_file).stem + '.csv'
        self.current_anno = Path(self.menubar.proj_anno_path)/anno_file
        self.statusbar.update()
        self.imagecanvas.draw_loaded_roi()

    def set_file2canvas_failed(self):
        messagebox.showerror("Failed to load files", 
                "Make sure you've opened the valid project folder")
        self.statusbar.update()


class AppMenu(tk.Menu):
    def __init__(self, root):
        super().__init__(root)
        self.root = root
        # File menu bar
        filemenu = tk.Menu(self, tearoff=True)
        self.add_cascade(label='File', 
                underline=0,
                menu=filemenu)
        filemenu.add_command(label='Open project folder', 
           underline=0,
           command=self.open_proj,
           accelerator='Ctrl+O')
        filemenu.add_command(label='Save as', 
           underline=1,
           command=self.saveas_file,
           accelerator='Ctrl+S')
        filemenu.entryconfig("Save as", state='disabled') #TODO[save_as]
        filemenu.add_separator()
        filemenu.add_command(label='Exit', 
                underline=1,
                #command=quit,
                command=self.root.check_before_quit,
                accelerator='Ctrl+Q')
        # Help menu bar
        helpmenu = tk.Menu(self, tearoff=True)
        helpmenu.add_command(label='Usage', command=self.usage_info)
        helpmenu.add_separator()
        helpmenu.add_command(label='About', command=self.about)
        helpmenu.add_command(label='Visit the site...', command=self.visit_site)
        self.add_cascade(label='Help', menu=helpmenu)

    def usage_info(self):
        messagebox.showinfo('Usage', Info.usage_text)

    def about(self):
        messagebox.showinfo('About', Info.about_text)

    def visit_site(self):
        webbrowser.open(r'https://gitlab.com/hiankun/tklabeller', new=0)

    #TODO[save_as]: Re-think this function. It's useless so far.
    def saveas_file(self, event='saveas_file'):
        save_anno_csv = asksaveasfile(title='Save annotation file as',
                initialdir='./',
                filetypes = (
                    ("csv files","*.csv"),
                    ("xml files","*.xml"),
                    ("all files","*.*"))
                )
        save_anno_csv.write(str(self.root.imagecanvas.roi_pts))
        save_anno_csv.close()

    def open_proj(self, event='open_proj'):
        if not self.root.imagecanvas.clear_roi():
            return

        # reset the proj_path to the default one
        config = reset_config()
        Common.proj_path = config['proj_path']

        self.root.img_list = []
        self.root.img_file = None
        if Common.start_from_last:
            self.root.f_idx = Common.last_f_idx
        else:
            self.root.f_idx = 0
        _p = Path(Common.proj_path)
        if _p.is_dir():
            ret = messagebox.askyesno("Default project founded", 
                    f"Found default project path '{Common.proj_path}'."
                    + " Open it?"
                    + "\n\n(Choose 'No' to open other project...)")
            if ret is True:
                pass
            else:
                Common.proj_path = askdirectory(title='Select proj folder',
                        initialdir=Path.cwd())
        else:
            Common.proj_path = askdirectory(title='Select proj folder',
                    initialdir=Path.cwd())
        logger.info(f'open_proj: open {Common.proj_path}...')

        self.proj_img_path = Path(Common.proj_path)/Common.img_dir
        self.proj_anno_path = Path(Common.proj_path)/Common.anno_dir
        self.proj_label_file = Path(Common.proj_path)/Common.label_filename
        
        for ext in Common.img_ext:
            self.root.img_list.extend(
                    Path(self.proj_img_path).glob(ext))
        self.root.img_list = sorted([str(_p) for _p in self.root.img_list])
        self.root.f_len = len(self.root.img_list)
        # load the label file
        self.root.listboxframe.load_label()
        try:
            self.root.set_file2canvas()
            self.root.listboxframe.load_img_list()
        except:
            self.root.set_file2canvas_failed()


class StatusBar(tk.Frame):
    def __init__(self, root):
        super().__init__(root)
        self.root = root

        self.show_toolname = tk.Label(self,
                background=Draw.status_color[0][0],
                foreground=Draw.status_color[0][1],
                text='T:None')
        self.show_toolname.pack(side='left')

        self.show_imginfo = tk.Label(self, 
                background=Draw.status_color[1][0],
                foreground=Draw.status_color[1][1],
                text='None')
        self.show_imginfo.pack(side='left',fill='x',expand=False)

        self.show_filename = tk.Label(self, 
                background=Draw.status_color[2][0],
                foreground=Draw.status_color[2][1],
                text='Image filename will be shown here...')
        self.show_filename.pack(side='left',fill='x',expand=True)

        self.show_labelinfo = tk.Label(self, 
                background=Draw.status_color[0][1],
                foreground=Draw.status_color[0][0],
                text='None')
        self.show_labelinfo.pack(side='left',fill='x',expand=False)

        self.duration = tk.StringVar()
        self.start = time.time()
        self.show_durationinfo = tk.Label(self, 
                background=Draw.status_color[1][0],
                foreground=Draw.status_color[1][1],
                textvariable=self.duration)
        self.show_durationinfo.pack(side='left',fill='x',expand=False)
        self.update_duration()

    def get_duration(self):
        duration_sec = int(time.time() - self.start)
        return datetime.timedelta(seconds=duration_sec) 
        
    def update_duration(self):
        self.duration.set(self.get_duration())
        self.after(1000, self.update_duration)

    def update(self, event='update'):
        self.show_toolname.config(text=
                f'T:{self.root.toolpanel.current_tool.get()}')
        self.show_imginfo.config(text=
                f'({self.root.imagecanvas.img_w},{self.root.imagecanvas.img_h})')
        if self.root.img_file is None or not self.root.img_list:
            file_status = 'No file loaded...'
        else:
            file_status = f'[{self.root.f_idx+1}/{self.root.f_len}]' \
                          f'{self.root.img_file}'
        if self.root.imagecanvas.anno_dict_changed:
            file_status += '*'
        self.show_filename.config(text=file_status)

        self.show_labelinfo.config(
                text=f'Obj:{self.root.listboxframe.total_count()}')


class ToolPanel(tk.Frame):
    def __init__(self, root):
        super().__init__(root)
        self.root = root
        toolpanel_label = tk.Label(self, text='Annotation Tools')
        toolpanel_label.pack(side='top',ipadx=0,ipady=5,fill='x')

        self.current_tool = tk.IntVar()
        self.current_tool.set(0)
        for text, mode in Common.tool_set:
            self.tool = tk.Radiobutton(self, text=text,
                    command=self.root.statusbar.update,
                    variable=self.current_tool, value=mode)
            self.tool.pack(side='top',anchor='w')

        toolpanel_info = tk.Label(self,relief=tk.GROOVE,justify=tk.LEFT,
                font=('Courier',11), text=Info.tool_info)
        toolpanel_info.pack(side='top',pady=(10,0),ipadx=10,ipady=10,
                fill='both',expand=True)


class ListboxFrame(tk.Frame):
    # ref:
    # http://effbot.org/tkinterbook/listbox.htm
    # https://stackoverflow.com/questions/24656138/python-tkinter-attach-
    #         scrollbar-to-listbox-as-opposed-to-window
    
    def __init__(self, root):
        super().__init__(root)
        self.root = root
        # Labels to be selected
        _label = tk.Label(self,text='Class Labels')
        _label.grid(row=0,column=0,columnspan=2)
        self.labellistbox = tk.Listbox(self, selectmode=tk.SINGLE,
                selectbackground='red',
                selectforeground='yellow',
                font=('Courier',11))
        self.labelvbar = tk.Scrollbar(self, orient='vertical',
                command=self.labellistbox.yview)
        self.labelvbar.grid(row=1,column=1,sticky='ns')
        self.labellistbox.config(yscrollcommand=self.labelvbar.set)
        self.labellistbox.grid(row=1,column=0)

        self.loadlabelbutton = tk.Button(self, text='Load labels',
                command=lambda:self.load_label(autoload=False))
        self.loadlabelbutton.grid(row=2,column=0,columnspan=2)

        # Files to be selected
        _label = tk.Label(self,text='Image Files')
        _label.grid(row=3,column=0,columnspan=2)
        self.filelistbox = tk.Listbox(self, selectmode=tk.SINGLE,
                selectbackground='black',
                selectforeground='white',
                )
        self.filelistbox.bind('<Double-Button-1>', self.select_image)
        self.filevbar = tk.Scrollbar(self, orient='vertical',
                command=self.filelistbox.yview)
        self.filevbar.grid(row=4,column=1,sticky='ns')
        self.filelistbox.config(yscrollcommand=self.filevbar.set)
        self.filelistbox.grid(row=4,column=0,sticky='ns')
        self.rowconfigure(4, weight=1)
    
        self.label_list = []
        self.anno_dict = {} # store the annotated info
        active_label_idx = 0

    def get_label_idx(self):
        #return self.labellistbox.curselection()[0]
        return self.active_label_idx 

    def set_label_focus(self):
        self.labellistbox.select_set(self.get_label_idx())

    def get_label(self):
        self.active_label_idx = self.labellistbox.curselection()[0]
        return self.label_list[self.active_label_idx]
    
    def total_count(self):
        label_counts = self.obj_stat()
        return sum(label_counts.values())

    def obj_stat(self):
        try:
            anno_files = Path(self.root.menubar.proj_anno_path).glob('*.csv')
        except:
            return None
        
        l_list = []
        for anno_file in anno_files:
            with open(anno_file, 'r') as f:
                anno_data = csv.reader(f, delimiter=',')
                for _data in anno_data:
                    l_list.append(_data[3])
        return Counter(l_list)

    def config_label_color(self):
        for label in self.label_list:
            label_idx = self.label_list.index(label)
            _color = Draw.roi_line_color[label_idx]
            self.labellistbox.itemconfig(label_idx, background=_color,
                    foreground=Draw.hex_inverter(_color))

    def update_label_counts(self):
        # FIX: It counts all labels through all the annotation files 
        # whenever they are saved. Maybe there're smarter ways.
        try:
            self.active_label_idx = self.labellistbox.curselection()[0]
        except:
            self.active_label_idx = 0

        self.labellistbox.delete(0, tk.END)
        label_counts = self.obj_stat()
        for label in self.label_list:
            self.labellistbox.insert(tk.END, 
                    f'{label_counts[label]:4d}:{label}')
        self.config_label_color()
        #self.labellistbox.select_set(self.active_label_idx)
        self.set_label_focus()

    def load_label(self, autoload=True):
        if not autoload:
            _label_filename = askopenfilename(title='Load Labels', 
                    initialdir=Common.proj_path)
        else:
            _label_filename = self.root.menubar.proj_label_file
        self.label_list.clear()
        try:
            with open(_label_filename) as label_file:
                self.label_list = [label.rstrip() for label in label_file]
        except:
            logger.warning(f'load_label: no file named {_label_filename}')
            return

        self.labellistbox.delete(0, tk.END)
        label_counts = self.obj_stat()
        for label in self.label_list:
            if label_counts:
                self.labellistbox.insert(tk.END, 
                        f'{label_counts[label]:4d}:{label}')
            else:
                self.labellistbox.insert(tk.END, f'{0:4d}:{label}')
        self.config_label_color()
        self.labellistbox.select_set(0)

    def select_image(self, event):
        # Note: 
        # Get the f_idx before the clear_roi() test, or the curselection() will 
        # fail because the focus has been changed to the pop up dialog.

        self.root.f_idx = self.filelistbox.curselection()[0]
        if not self.root.imagecanvas.clear_roi():
            return
        self.root.set_file2canvas()
        # TODO: Is it better to set the focus back to labellistbox after
        # selecting image file from filelistbox?
        #self.set_label_focus()
    
    def load_img_list(self):
        self.filelistbox.delete(0, tk.END)
        for _, img_filename in enumerate(self.root.img_list):
            self.filelistbox.insert(tk.END, 
                    f'{_+1} {Path(img_filename).name}')


class ImageCanvas(tk.Frame):
    def __init__(self, root):
        super().__init__(root)
        self.root = root
        self.canvas = tk.Canvas(self,
                background=Draw.canvas_bg_color, cursor='crosshair')
        self.canvas.bind_all('<Key>', self.key_pressed)
        #self.canvas.focus_set() #use `bind_all` above and don't need this
        self.canvas.bind('<Motion>', self.cursor_move_draw)
        self.canvas.bind('<ButtonPress-1>', self.set_points)
        self.canvas.bind_all('<Escape>', self.cancel_set_points)
        self.canvas.bind('<ButtonPress-3>', self.click_on_obj)
        self.canvas.bind('<Double-ButtonPress-3>', self.change_label)
        self.img_on_canvas = self.canvas.create_image(
                0,0,image=None,anchor='nw')#, state=tk.DISABLED)
        self.canvas.pack(side='left', fill='both', expand=True)

        # Scroll
        self.hbar = tk.Scrollbar(self.canvas, orient='horizontal',
                command=self.canvas.xview)
        self.hbar.pack(side='bottom', fill='x')
        self.vbar = tk.Scrollbar(self.canvas, orient='vertical',
                command=self.canvas.yview)
        self.vbar.pack(side='right', fill='y')
        self.canvas.config(xscrollcommand=self.hbar.set, 
                yscrollcommand=self.vbar.set)
        self.canvas.bind_all(Control.canvas_left, 
                lambda event: self.canvas.xview_scroll(-1,'units'))
        self.canvas.bind_all(Control.canvas_down, 
                lambda event: self.canvas.yview_scroll( 1,'units'))
        self.canvas.bind_all(Control.canvas_up, 
                lambda event: self.canvas.yview_scroll(-1,'units'))
        self.canvas.bind_all(Control.canvas_right, 
                lambda event: self.canvas.xview_scroll( 1,'units'))

        ## Zooming: TODO
        ## for Windows
        #self.canvas.bind('<MouseWheel>', self.mouse_wheel_zoom)
        ## for Linux
        #self.canvas.bind('<Button-4>', self.mouse_wheel_zoom)
        #self.canvas.bind('<Button-5>', self.mouse_wheel_zoom)

        # ROI and GrabCut
        self.roi_pts = []
        self.sqaure_radius = None
        self.gc_rect = None
        self.gc_mask = None
        self.gc_res_mask = None
        self.gc_polygon = None
        # Original and scaled pil image parts
        self.pil_img = None
        self.pil_mask = None
        self.scaled_pil_img = None
        self.scaled_pil_mask = None
        # Scale
        self.scale = 1.0
        self.delta = 0.1

    def mouse_wheel_zoom(self, event='mouse_wheel_zoom'):
        if event.num == 4:
            self.zoom(delta=self.delta)
        if event.num == 5:
            self.zoom(delta=-self.delta)

    def key_pressed(self, event='key_pressed'):
        self.kp = event.char
        if self.kp == Control.kill_roi:
            self.kill_roi()
        if self.kp == Control.update_anno:
            self.save_annotations()
        if self.kp == GrabCut.do_grabcut:
            self.do_grabcut()
        if self.kp == GrabCut.add_gc_poly:
            self.roi_pts = [int(_) for _ in self.gc_polygon.flatten()]
            self.draw_roi(self.roi_pts)
            self.clear_gc_stuff()
        if self.kp == 'x':
            print('this is for temp tests...')

    def kill_roi(self):
        for roi2del in Draw.selected_roi:
            logger.debug(f'roi2del: {roi2del}')
            logger.info('delete selected roi: '
                    f'{self.canvas.gettags(roi2del)}:{roi2del}')
            for tag in [roi2del, f'label_rect_{roi2del}', f'label_text_{roi2del}']:
                self.canvas.delete(tag)
            self.root.listboxframe.anno_dict.pop(roi2del)
            self.root.statusbar.update()
        Draw.selected_roi = []

    def do_grabcut(self):
        mode = self.root.toolpanel.current_tool.get()
        if not(mode in [Common.grabcut_mode['GrabCut'],
            Common.grabcut_mode['BG'], Common.grabcut_mode['FG'],
            Common.grabcut_mode['PR_BG'], Common.grabcut_mode['PR_FG']]):
            messagebox.showerror("GrabCut Error",
                    "You need to select GrabCut mode first...")
        _mode_key = list(Common.grabcut_mode.keys())[
                list(Common.grabcut_mode.values()).index(mode)]
        logger.info(f'GrabCut with mode "{_mode_key}"')
        self.gc_mask, self.gc_res_mask, self.gc_polygon = \
                self.root.gc.cal_mask(mode=mode, ref_rect=self.gc_rect, 
                mask=self.gc_mask, img_file=self.root.img_file)
        if self.gc_polygon is None: 
            messagebox.showinfo('Oops from GrabCut',
                    "GrabCut failed to detect FG. Set it manually...")
            return
        for tag in ['gc_rect', 'temp_line_seg']:
            #FIX: don't delete 'gc_rect' here...
            self.canvas.delete(tag)
        self.show_gc_res_mask()
        self.create_gc_poly()

    def zoom(self, delta=0.1):
        self.scale += delta
        if self.scale < self.delta:
            self.scale = self.delta
        logger.info(f'zoom: {self.scale}')

        self.load_image(self.root.img_file)
        try:
            self.scale_ref_rect()
        except:
            logger.debug('zoom: no ref_rect to be rescaled...')
        try:
            self.draw_poly() #TODO
        except:
            logger.debug('zoom: no polygon to be rescaled...')

        if self.pil_mask:
            self.show_gc_res_mask()
        else:
            logger.debug('zoom: no mask to be rescaled...')

    def show_gc_res_mask(self):
        _alpha = 0.5
        #_mask = np.stack((Common.gc_res_mask,)*4, axis=-1)
        _mask = np.stack((self.gc_res_mask,)*4, axis=-1)
        _r, _g, _b = 255,255,0
        np_mask = np.where(_mask==(255,255,255,255),
                (_r,_g,_b,int(_alpha*255)), (0,0,0,0)).astype(np.uint8)
        pil_mask = Image.fromarray(np_mask)
        #blended = Image.alpha_composite(self.pil_img.convert('RGBA'), pil_mask)
        pil_img = self.pil_img.convert('RGBA')
        blended = Image.alpha_composite(pil_img, pil_mask)
        self.tk_mask = ImageTk.PhotoImage(blended)
        self.canvas.itemconfig(self.img_on_canvas, image=self.tk_mask)

    def create_gc_poly(self):
        self.canvas.delete('gc_poly')
        self.roi_pts = [int(_) for _ in self.gc_polygon.flatten()]
        logger.debug('GrabCut Polygon: ', self.roi_pts)
        self.canvas.create_polygon(self.roi_pts, tags='gc_poly', 
                outline=Draw.selected_line_color, width=2, fill='')
        self.roi_pts.clear()

    def clear_gc_stuff(self):
        for tag in ['gc_rect', 'gc_poly']:
            self.canvas.delete(tag)
        self.roi_pts.clear()
        self.canvas.itemconfig(self.img_on_canvas, image=self.img)

    def is_close(self, pt0, pt1):
        try:
            p0x, p0y = pt0
            p1x, p1y = pt1
        except:
            return False
        if ( abs(p0x-p1x) < Common.closeness 
                and abs(p0y-p1y) < Common.closeness ):
            return True
        return False

    def get_canvas_xy(self, event):
        x = self.canvas.canvasx(event.x)
        y = self.canvas.canvasy(event.y)
        x,y = self.limit_pt_in_canvas(x,y)
        return x,y

    def limit_pt_in_canvas(self, x, y):
        if x < 0: x = 0
        if y < 0: y = 0
        if x > self.img_w: x = self.img_w
        if y > self.img_h: y = self.img_h
        return x,y

    def set_points(self, event):
        x,y = self.get_canvas_xy(event)
        self.roi_pts.extend([x,y])

        mode = self.root.toolpanel.current_tool.get()
        len_pts = len(self.roi_pts)
        if mode == Common.tool_mode['Rectangle']:
            if len_pts >= 4:
                self.draw_roi(self.roi_pts)
                self.roi_pts.clear()
            if not self.roi_pts:
                Draw.draw_guiding_cross = True
            else:
                Draw.draw_guiding_cross = False
        elif mode == Common.tool_mode['Square']:
            if len_pts >= 4:
                x1,y1,x2,y2 = self.roi_pts
                r = self.sqaure_radius
                pts = [x1-r, y1-r, x1+r, y1+r]
                self.draw_roi(pts)
                self.roi_pts.clear()
        elif mode == Common.tool_mode['FixedSquare']:
            if len_pts >= 2:
                x,y = self.roi_pts
                r = self.sqaure_radius
                pts = [x-r, y-r, x+r, y+r]
                self.draw_roi(pts)
                self.roi_pts.clear()
        elif mode == Common.tool_mode['Polygon']:
            if len_pts >= 8:
                start = self.roi_pts[:2]
                end = self.roi_pts[-2:]
                if self.is_close(start, end):
                    self.roi_pts = self.roi_pts[:-2]
                    self.draw_roi(self.roi_pts)
                    self.roi_pts.clear()
        elif mode == Common.grabcut_mode['GrabCut']:
            if len_pts >= 4:
                self.canvas.delete('gc_rect')
                _id = self.canvas.create_rectangle(self.roi_pts,
                        tags=('gc_rect'),
                        outline=Draw.roi_line_color[
                            self.root.listboxframe.active_label_idx], 
                        width=Draw.roi_line_width, 
                        )
                self.gc_rect = [int(c) #int(c/self.scale) 
                        for c in self.canvas.coords(_id)]
                self.roi_pts.clear()
                if Common.auto_grabcut: self.do_grabcut()
            #return
            if not self.roi_pts:
                Draw.draw_guiding_cross = True
            else:
                Draw.draw_guiding_cross = False
        elif mode == Common.grabcut_mode['BG']:
            self.set_gc_mask(Draw.mask_BG['color'], Draw.mask_BG['val'])
        elif mode == Common.grabcut_mode['FG']:
            self.set_gc_mask(Draw.mask_FG['color'], Draw.mask_FG['val'])
        elif mode == Common.grabcut_mode['PR_BG']:
            self.set_gc_mask(Draw.mask_PR_BG['color'], Draw.mask_PR_BG['val'])
        elif mode == Common.grabcut_mode['PR_FG']:
            self.set_gc_mask(Draw.mask_PR_FG['color'], Draw.mask_PR_FG['val'])
        else:
            pass

    def set_gc_mask(self, fill_color, mask_val):
        if len(self.roi_pts) >=4:
            end1 = self.roi_pts[-4:-2]
            end2 = self.roi_pts[-2:]
            if self.is_close(end1, end2):
                self.roi_pts = self.roi_pts[:-2]
                self.fine_touch_mask(self.roi_pts, fill_color, mask_val)
                self.roi_pts.clear()
                if Common.auto_grabcut: self.do_grabcut()

    def fine_touch_mask(self, pts, fill_color, mask_val):
        r = Draw.brush_size
        # For vision purpose only
        self.canvas.create_line(pts, tags='temp_line_seg', fill=fill_color, width=r)
        # The following drawing won't show on the screen but set real mask pixels
        _mask_img = Image.fromarray(self.gc_mask)
        _mask_draw = ImageDraw.Draw(_mask_img)
        _mask_draw.line(pts, fill=mask_val, width=r)
        self.gc_mask = np.array(_mask_img)

    def cancel_set_points(self, event='cancel_set_points'):
        # Drop the last point from the roi_pts.
        self.roi_pts = self.roi_pts[:-2]
    
        mode = self.root.toolpanel.current_tool.get()
        if mode in [Common.tool_mode['Rectangle'], Common.grabcut_mode['GrabCut']]:
            Draw.draw_guiding_cross = True
        if mode in [Common.tool_mode['Polygon'], 
                Common.grabcut_mode['BG'], Common.grabcut_mode['FG'],
                Common.grabcut_mode['PR_BG'], Common.grabcut_mode['PR_FG'],
                ] and self.roi_pts:
            # Clear old temp lines as well as segs and then create a new seg 
            # which has dropped the cancelled point.
            self.canvas.delete('temp_line')
            self.canvas.delete('temp_line_seg')
            try:
                self.canvas.create_line(self.roi_pts,
                        tags='temp_line_seg',
                        fill=Draw.roi_line_color[
                            self.root.listboxframe.active_label_idx], 
                        width=Draw.roi_line_width)
            except:
                pass
        
    def popuplabel_destroy(self, event):
        #FIX: do we have better way to exit?
        self.popuplabel.destroy()

    def set_changed_label(self, event):
        #_lablelistbox_idx = self.root.listboxframe.get_label_idx()
        new_label_idx = self.poplabellistbox.curselection()[0]
        new_label = self.root.listboxframe.label_list[new_label_idx]
        new_selected_roi = []
        for roi_id in Draw.selected_roi:
            orig_label = self.root.listboxframe.anno_dict[roi_id][3]
            #self.root.listboxframe.set_label_focus(_lablelistbox_idx)
            self.root.listboxframe.set_label_focus()
            if new_label == orig_label:
                return
            else:
                orig_pts = self.root.listboxframe.anno_dict[roi_id][4:]
                orig_label_xy = orig_pts[:2] #Common.anno_dict[roi_id][4:6]
                label_rect_id = self.canvas.find_withtag(
                        f'label_rect_{roi_id}')
                label_text_id = self.canvas.find_withtag(
                        f'label_text_{roi_id}')
                # remove old roi in anno_dict
                self.root.listboxframe.anno_dict.pop(roi_id)
                # update canvas
                for _ in [roi_id, label_rect_id, label_text_id]:
                    self.canvas.delete(_)
                new_id = self.draw_roi(orig_pts, label=new_label)
                new_selected_roi.append(new_id)
                logger.debug('label updated in anno_dict '
                        f'{{{new_id}:{self.root.listboxframe.anno_dict[new_id]}}}')
        Draw.selected_roi = new_selected_roi

    def change_label(self, event):
        self.popuplabel = tk.Toplevel(self)
        self.popuplabel.title("Change label")
        self.popuplabel.geometry(
                '%dx%d+%d+%d' % (100,120,event.x_root,event.y_root))
        self.popuplabel.bind('<Escape>', self.popuplabel_destroy)
        self.poplabellistbox = tk.Listbox(
                self.popuplabel, selectmode=tk.SINGLE, font=('Courier',10))
        self.poplabellistbox.bind('<Double-Button-1>', self.set_changed_label)

        vbar = tk.Scrollbar(self.popuplabel,orient='vertical',
                command=self.poplabellistbox.yview)
        vbar.pack(side='right',fill='y',expand=True)
        self.poplabellistbox.config(yscrollcommand=vbar.set)
        self.poplabellistbox.pack(side='left')
        self.poplabellistbox.delete(0, tk.END)
        for label in self.root.listboxframe.label_list:
            self.poplabellistbox.insert(tk.END, f'{label}')

    def save_annotations(self):
        with open(self.root.current_anno, 'w') as f:
            for anno in self.root.listboxframe.anno_dict.values():
                f.write(','.join(str(_) for _ in anno) + '\n')
        logger.info(f'{self.root.current_anno} saved...')
        self.root.statusbar.update()
        self.root.listboxframe.update_label_counts()

    @property
    def anno_dict_changed(self):
        loaded_roi = self.load_roi()
        screen_roi = list(self.root.listboxframe.anno_dict.values())
        if loaded_roi:
            roi_in_file = sorted(loaded_roi) 
            roi_on_screen = sorted(screen_roi)
            return roi_in_file != roi_on_screen
        elif not loaded_roi and screen_roi:
            return True
        else:
            return False

    def clear_roi(self):
        logger.debug(f'clear_roi: {self.root.listboxframe.anno_dict}')
        if self.anno_dict_changed:
            ret = messagebox.askyesnocancel("ROI changed", 
                "Some annotations have been changed. Save them?"
                + "(Cancel to stay here...)")
            if ret is True:
                logger.debug('clear_roi: save then clear anno_dict')
                self.save_annotations()
            elif ret is False:
                logger.debug('clear_roi: clear unsaved anno_dict')
            else: # user cancel it; stay in the same frame
                return False

        self.root.listboxframe.anno_dict.clear()
        self.roi_pts.clear()
        for tag in ['rect','poly','label_rect','label_text',
                'temp_line','temp_line_seg','gc_rect','gc_poly']:
            self.canvas.delete(tag)
        return True

    def load_roi(self):
        try:
            loaded_roi = []
            with open(self.root.current_anno, 'r') as f:
                anno_data = csv.reader(f, delimiter=',')
                for _data in anno_data:
                    img_filename,_w,_h,label, *_pts = _data
                    w,h = int(_w),int(_h)
                    pts = [int(_) for _ in _pts]
                    data = [img_filename,w,h,label]
                    data.extend(pts)
                    loaded_roi.append(data)
            return loaded_roi
        except:
            return None

    def draw_loaded_roi(self):
        self.root.listboxframe.anno_dict.clear()
        loaded_roi = self.load_roi()
        if loaded_roi:
            for _roi in loaded_roi:
                _img_filename,_w,_h,label, *loaded_pts = _roi
                label_idx = self.root.listboxframe.label_list.index(label)
                if len(loaded_pts) == 4: # For 'rect' and 'sqau'
                    logger.debug(f'draw_loaded_roi: rect:{loaded_pts}')
                    tags = 'rect'
                    roi_id = self.canvas.create_rectangle(loaded_pts, 
                            tags=tags,
                            outline=Draw.roi_line_color[label_idx], 
                            width=Draw.roi_line_width, 
                            )
                elif len(loaded_pts) > 4: # For 'poly'
                    logger.debug(f'draw_loaded_roi: poly:{loaded_pts}')
                    tags = 'poly'
                    roi_id = self.canvas.create_polygon(loaded_pts, 
                            tags=tags,
                            outline=Draw.roi_line_color[label_idx], 
                            width=Draw.roi_line_width, 
                            fill='')
                else:
                    logger.warning('draw_loaded_roi: Unknown condition')

                self.canvas.addtag_withtag(f'{label}', roi_id)
                self.draw_label(loaded_pts[:2], label, roi_id)
                # Add the ROIs to anno_dict for later usage
                self.root.listboxframe.anno_dict[roi_id] = [self.root.img_file,
                        self.img_w,self.img_h,label] + loaded_pts
            self.root.statusbar.update()

    def draw_label(self, pts, label, roi_id):
        if Draw.enable_label_visible:
            state = 'normal'
        else:
            state = 'hidden'
        # FIX: The following hard coded sizes parameters were determined by 
        # trial and error. Maybe there're smarter ways to do it.
        _x,_y = pts
        _w = 7*len(label)
        _h = 12
        _y += _h #int(_h/2)

        # NOTE: Don't begin tag id with digitals.
        # ref: http://tcl.tk/man/tcl8.5/TkCmd/canvas.htm
        # See the section of "ITEM IDS AND TAGS".
        
        label_idx = self.root.listboxframe.label_list.index(label)
        _color = Draw.roi_line_color[label_idx]
        label_rect_id = self.canvas.create_rectangle(_x,_y,_x+_w,_y-_h, 
                tags=('label_rect'), fill=_color, width=0, state=state)
        self.canvas.addtag_withtag(f'label_rect_{roi_id}', label_rect_id)
        label_text_id = self.canvas.create_text(_x,_y, anchor='sw', 
                tags=('label_text'), font=('Courier',8), 
                text=label, fill=Draw.hex_inverter(_color), state=state)
        self.canvas.addtag_withtag(f'label_text_{roi_id}', label_text_id)
        #self.canvas.addtag_withtag(f'label_text:{roi_id}:{label}', label_text_id)

    def draw_roi(self, pts, label=None):
        if label is None:
            try:
                label = self.root.listboxframe.get_label() 
            except:
                messagebox.showerror("No label selected", 
                    "Select a label in Class Labels")
                return
        label_idx = self.root.listboxframe.label_list.index(label)

        mode = self.root.toolpanel.current_tool.get()
        if len(pts) == 4:
            if mode in [Common.tool_mode['Rectangle'], 
                    Common.tool_mode['Square'],
                    Common.tool_mode['FixedSquare'],
                    ]:
                roi_id = self.canvas.create_rectangle(pts, tags=('rect'),
                    outline=Draw.roi_line_color[label_idx], 
                    width=Draw.roi_line_width, 
                    )
            else:
                logger.warning('draw_roi: Unknown mode') #FIX
        elif len(pts) > 4:
            roi_id = self.canvas.create_polygon(pts, tags=('poly'),
                    outline=Draw.roi_line_color[label_idx], 
                    width=Draw.roi_line_width, 
                    fill='')
            self.canvas.delete('temp_line_seg')
        else:
            logger.warning('draw_roi: Unknown mode') #FIX
        self.canvas.addtag_withtag(f'{label}', roi_id)
        
        # NOTE: `pts` stores the clicking order which might be not in the form
        # of "up-left to bottom-right". Therefore, use `ordered_pts` instead.
        ordered_pts = [int(_) for _ in self.canvas.coords(roi_id)]
        self.draw_label(ordered_pts[:2], label, roi_id)
        self.root.listboxframe.anno_dict[roi_id] = [self.root.img_file,
                self.img_w,self.img_h,label] + ordered_pts
        self.root.statusbar.update()
        return roi_id

    def get_limited_radius(self, x,y,w,h):
        # Make sure that the radius won't exceed the canvas
        _x = w-x
        _y = h-y
        return min(x,y,_x,_y)

    def cursor_move_draw(self, event):
        # Use canvasx() and canvasy() to convert x and y from window to canvas 
        # coordinates. 
        # ref: https://stackoverflow.com/a/11310847
        
        try:
            mode = self.root.toolpanel.current_tool.get()
            x,y = self.get_canvas_xy(event)
            w = self.img_w
            h = self.img_h
            label = self.root.listboxframe.get_label() 
            label_idx = self.root.listboxframe.label_list.index(label)
            _color = Draw.roi_line_color[label_idx]
        except:
            return

        if mode in [Common.tool_mode['Rectangle'], Common.grabcut_mode['GrabCut']]:
            for tag in ['guiding_cross', 'temp_rect']:
                self.canvas.delete(tag)
            if Draw.draw_guiding_cross and Draw.enable_guiding_cross:
                self.canvas.create_line(0,y, w,y, x,y, x,0, x,h,
                        tags='guiding_cross', 
                        fill=Draw.cross_line_color, 
                        width=Draw.cross_line_width)
            if len(self.roi_pts) == 2: # only one point has been set
                _x,_y = self.roi_pts
                self.canvas.create_rectangle(_x,_y,x,y, 
                        tags='temp_rect', 
                        outline=_color, width=Draw.roi_line_width)
        elif mode == Common.tool_mode['Square']:
            for tag in ['guiding_cross', 'temp_oval']:
                self.canvas.delete(tag)
            if self.roi_pts:
                # Center circle for visual guidance
                _x0,_y0 = self.roi_pts[:2]
                _r = Common.closeness #FIX: use another size?
                self.canvas.create_oval(_x0-_r,_y0-_r,_x0+_r,_y0+_r,
                        tags='temp_oval',
                        fill=_color, width=0)
                # Draw the circular boundary
                limited_r = self.get_limited_radius(_x0,_y0,w,h)
                _br = math.sqrt((_x0 - x)**2 + (_y0 - y)**2)
                br = _br if _br < limited_r else limited_r
                self.sqaure_radius = br
                self.canvas.create_oval(_x0-br,_y0-br,_x0+br,_y0+br,
                        tags='temp_oval',
                        outline=_color,
                        width=Draw.roi_line_width, dash=(2,6))
        elif mode == Common.tool_mode['FixedSquare']:
            for tag in ['guiding_cross', 'temp_oval']:
                self.canvas.delete(tag)
            # Draw the circular boundary
            limited_r = self.get_limited_radius(x,y,w,h)
            _br = Common.fixed_radius #FIX
            br = _br if _br < limited_r else limited_r
            self.sqaure_radius = br
            self.canvas.create_oval(x-br,y-br,x+br,y+br,
                    tags='temp_oval',
                    outline=_color,
                    width=Draw.roi_line_width, dash=(2,6))
        elif mode == Common.tool_mode['Polygon']:
            for tag in ['guiding_cross', 'temp_oval', 'temp_line']:
                self.canvas.delete(tag)
            if self.roi_pts:
                # Referece circle for closeness visual guidance
                _x0,_y0 = self.roi_pts[:2]
                _r = Common.closeness
                self.canvas.create_oval(_x0-_r,_y0-_r,_x0+_r,_y0+_r,
                        tags='temp_oval', # i'm lazy to use new variable name...
                        fill=_color, width=0)
                self.draw_line_seg(x, y, _color, Draw.roi_line_width)
        elif mode in [Common.grabcut_mode['BG'], Common.grabcut_mode['FG'],
                Common.grabcut_mode['PR_BG'], Common.grabcut_mode['PR_FG'],
                ]:
            for tag in ['guiding_cross', 'temp_oval', 'temp_line']:
                self.canvas.delete(tag)
            if self.roi_pts:
                self.draw_line_seg(x, y, _color, Draw.brush_size)
        else:
            pass

    def draw_line_seg(self, x, y, color, width):
        # Draw the following line segments
        _x,_y = self.roi_pts[-2:]
        self.canvas.create_line(_x,_y,x,y, tags='temp_line', 
                fill=color, width=width)
        if len(self.roi_pts) >= 4: # just to keep the drawn line segs
            self.canvas.create_line(self.roi_pts, tags='temp_line_seg',
                    fill=color, width=width)

    def resize_image(self, image, ratio):
        _w, _h = image.size
        return image.resize((int(_w*ratio), int(_h*ratio)))

    def get_image(self, img_filename, resize=False):
        #TODO: temporarily copy pil_img
        self.pil_img = Image.open(img_filename)
        pil_img = self.pil_img
        self.img_w, self.img_h = pil_img.size

        # TODO: The following code doesn't resize properly...
        if resize:
            _w, _ = pil_img.size
            if _w > self.root.width:
                pil_img = self.resize_image(pil_img, self.root.width/_w)
            _, _h = pil_img.size
            if _h > self.root.height:
                pil_img = self.resize_image(pil_img, self.root.height/_h)
        return ImageTk.PhotoImage(pil_img)

    def load_image(self, img_filename):
        # NOTE:
        # Use `self.img` instead of `img` so that it won't be released
        # ref: http://effbot.org/pyfaq/why-do-my-tkinter-images-not-appear.htm
        self.img = self.get_image(img_filename)
        self.canvas.itemconfig(self.img_on_canvas, image=self.img)
        self.canvas.config(scrollregion=(-30,-30,self.img_w+40, self.img_h+40))

    def toggle_color_selected(self, roi_id, selected=True):
        # selected: False to restore the original color and width.
        _label = self.root.listboxframe.anno_dict[roi_id][3]
        _xy = self.root.listboxframe.anno_dict[roi_id][4:6]
        if selected:
            _color = Draw.selected_line_color
            _text_color = Draw.selected_text_color
            _width = Draw.selected_line_width
        else:
            _label_idx = self.root.listboxframe.label_list.index(_label)
            _color = Draw.roi_line_color[_label_idx]
            _text_color = Draw.hex_inverter(_color)
            _width = Draw.roi_line_width

        self.canvas.itemconfig(roi_id, width=_width, outline=_color)
        label_rect_id = self.canvas.find_withtag(f'label_rect_{roi_id}')
        label_text_id = self.canvas.find_withtag(f'label_text_{roi_id}')
        self.canvas.itemconfig(label_rect_id, width=0, fill=_color)
        self.canvas.itemconfig(label_text_id, text=_label, fill=_text_color)

    def click_on_obj(self, event):
        x = self.canvas.canvasx(event.x)
        y = self.canvas.canvasy(event.y)

        Draw.selected_roi.clear()
        ids = []
        for tags in ['rect','poly']:
            ids += self.canvas.find_withtag(tags)
        for roi_id in ids:
            i_coords = self.canvas.coords(roi_id)
            inside = self.point_inside_polygon(x,y,i_coords)
            if inside:
                logger.debug('click_on_obj: clicked on '
                        f'{self.canvas.gettags(roi_id)}:{roi_id}')
                self.toggle_color_selected(roi_id)
                Draw.selected_roi.append(roi_id)
            else:
                # Restore other ROIs color
                self.toggle_color_selected(roi_id, selected=False)

        # Check if any polygons contains any others.
        # If any of it does, then it shouldn't be considered "selected."
        # FIX: The code is not efficient. It checks all pairs even when certian 
        # polygon has been removed from the list.

        if len(Draw.selected_roi) >= 2:
            to_check = list(itertools.permutations(Draw.selected_roi, 2))
            for target, others in to_check:
                target_coords = self.canvas.coords(target)
                others_coords = self.canvas.coords(others)
                include_others = self.polygon_include_others(
                        target_coords, others_coords)
                logger.debug('click_on_obj: '
                        f'{target} includes {others}:{include_others}')
                if include_others:
                    Draw.selected_roi.remove(target)
                    self.toggle_color_selected(target, selected=False)
        for roi_id in Draw.selected_roi:
            logger.info('click_on_obj: '
                    f'chosen {self.canvas.gettags(roi_id)}:{roi_id}')

    def polygon_include_others(self, target_coords, others_coords):
        others_coords = list(zip(others_coords[::2], others_coords[1::2]))
        for x,y in others_coords:
            include_others = self.point_inside_polygon(x,y, target_coords)
            if not include_others:
                return False
        return True

    def point_inside_polygon(self, x,y, coords):
        # src: http://www.ariel.com.au/a/python-point-int-poly.html
        # Test whether a point (x,y) is inside a polygon, # where polygon 
        # = [(x0,y0), (x1,y1),...]
        # TODO: I have to understand the math...

        c = coords
        if len(c) == 4: # it's a rect, has only 2 points
            # turn rect to 4 points polygon
            coords = [c[0],c[1],c[2],c[1],c[2],c[3],c[0],c[3]]
        poly = list(zip(coords[::2], coords[1::2]))

        n = len(poly)
        inside = False
    
        p1x,p1y = poly[0]
        for i in range(n+1):
            p2x,p2y = poly[i % n]
            if y > min(p1y,p2y):
                if y <= max(p1y,p2y):
                    if x <= max(p1x,p2x):
                        if p1y != p2y:
                            xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                        if p1x == p2x or x <= xinters:
                            inside = not inside
            p1x,p1y = p2x,p2y
    
        return inside


class NavigationPanel(tk.Frame):
    def __init__(self, root):
        super().__init__(root)
        self.root = root
        prev_btn = tk.Button(self, text='< prev', command=self.prev_file)
        next_btn = tk.Button(self, text='next >', command=self.next_file)
        prev_btn.pack(side='left',fill='x',expand=True)
        next_btn.pack(side='left',fill='x',expand=True)

    def prev_file(self, event='prev_file'):
        if not self.root.imagecanvas.clear_roi():
            return
        try:
            self.root.f_idx = self.root.f_idx-1 \
                    if self.root.f_idx>0 \
                    else self.root.f_idx
            self.root.set_file2canvas()
        except:
            self.root.set_file2canvas_failed()

    def next_file(self, event='next_file'):
        if not self.root.imagecanvas.clear_roi():
            return
        try:
            self.root.f_idx = self.root.f_idx+1 \
                    if self.root.f_idx<self.root.f_len-1 \
                    else self.root.f_idx
            self.root.set_file2canvas()
        except:
            self.root.set_file2canvas_failed()


if __name__ == '__main__':
    app = App()
    app.mainloop()
